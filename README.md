Getting started

1. Clone the project  
 Open up a terminal and run `git clone https://sorinilies@bitbucket.org/sorinilies/automation-learning.git`

2. Move to the newly created folder and run:
`npm install`

    If you don't have node install, get that first - https://www.npmjs.com/get-npm

3. Tests are placed in cypress/tests folder

4. `cypress open` - opens the cypress dashboard

5. Url is set to the e2e slot, use an existing user in your tests.
